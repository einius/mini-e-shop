<?php
namespace CartBundle\Service;
use CartBundle\Entity\Cart;
use CartBundle\Entity\CartRepository;
use CartBundle\Entity\RequestedToBuy;
use CartBundle\Entity\RequestedToBuyRepository;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use ItemBundle\Entity\Item;
use ItemBundle\Service\ItemService;
use NotificationBundle\Event\Events;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use UserBundle\Entity\User;

/**
 * Class BuyItemService
 * @package CartBundle\Service
 */
class BuyItemService
{
    /**
     * @var RequestedToBuyRepository
     */
    private $requestedToBuyRepository;

    /**
     * @var ItemService
     */
    private $itemService;

    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * BuyItemService constructor.
     * @param RequestedToBuyRepository $requestedToBuyRepository
     * @param ItemService $itemService
     * @param TokenStorage $tokenStorage
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(RequestedToBuyRepository $requestedToBuyRepository, ItemService $itemService, TokenStorage $tokenStorage, EventDispatcherInterface $dispatcher)
    {
        $this->requestedToBuyRepository = $requestedToBuyRepository;
        $this->itemService = $itemService;
        $this->tokenStorage = $tokenStorage;
        $this->dispatcher = $dispatcher;
    }


    /**
     * @param int $id
     * @return RequestedToBuy
     */
    public function buyItem($id)
    {
        $user = $this->getUser();
        $item = $this->itemService->getItem($id);
        $requestedToBuy = $user->getRequestedToBuy();
        $requestedToBuy->addItem($item);
        try {
            $this->requestedToBuyRepository->save($requestedToBuy);
            $this->dispatchEvent($item);
        } catch (UniqueConstraintViolationException $e) {
        }

        return $requestedToBuy;
    }

    /**
     * @return User
     */
    private function getUser()
    {
        return $this->tokenStorage->getToken()->getUser();
    }

    /**
     * @param Item $item
     */
    private function dispatchEvent(Item $item)
    {
        $event = new GenericEvent($item);
        $this->dispatcher->dispatch(Events::ITEM_REQUESTED_TO_BUY, $event);
    }
}
