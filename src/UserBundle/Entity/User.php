<?php

namespace UserBundle\Entity;

use CartBundle\Entity\Cart;
use CartBundle\Entity\RequestedToBuy;
use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use ItemBundle\Entity\Item;
use NotificationBundle\Entity\Notification;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Cart
     * @ORM\OneToOne(targetEntity="CartBundle\Entity\Cart", mappedBy="user", cascade={"persist"})
     */
    protected $cart;

    /**
     * @var RequestedToBuy
     * @ORM\OneToOne(targetEntity="CartBundle\Entity\RequestedToBuy", mappedBy="user", cascade={"persist"})
     */
    protected $requestedToBuy;

    /**
     * @ORM\OneToMany(targetEntity="NotificationBundle\Entity\Notification", mappedBy="notificationFor", fetch="EXTRA_LAZY")
     **/
    private $notifications;

    public function __construct()
    {
        parent::__construct();
        $this->notifications = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Cart
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * @param Cart $cart
     */
    public function setCart($cart)
    {
        $this->cart = $cart;
    }

    /**
     * @return RequestedToBuy
     */
    public function getRequestedToBuy()
    {
        return $this->requestedToBuy;
    }

    /**
     * @param RequestedToBuy $requestedToBuy
     */
    public function setRequestedToBuy($requestedToBuy)
    {
        $this->requestedToBuy = $requestedToBuy;
    }

    /**
     * @return Notification[]|ArrayCollection
     */
    public function getNotifications()
    {
        return $this->notifications;
    }

    /**
     * @param Notification[]|ArrayCollection $notifications
     */
    public function setNotifications($notifications)
    {
        $this->notifications = $notifications;
    }

    /**
     * @param Notification $notification
     */
    public function addNotification(Notification $notification) {
        $this->notifications->add($notification);
    }

    /**
     * @return int
     */
    public function getNotificationCount()
    {
        return $this->notifications->count();
    }
}
