<?php
namespace ItemBundle\Service;
use CoreBundle\Service\FileHandler;
use ItemBundle\Entity\Item;
use ItemBundle\Entity\ItemRepository;
use UserBundle\Entity\User;

/**
 * Class ItemService
 * @package ItemBundle\Service
 */
class ItemService
{
    const DEFAULT_SORT = 'search.id';

    const DEFAULT_SORT_DIRECTION = 'ASC';
    /**
     * @var ItemRepository
     */
    private $itemRepository;

    /**
     * @var FileHandler
     */
    private $fileHandler;

    /**
     * @var array
     */
    private $config;

    /**
     * ItemService constructor.
     * @param ItemRepository $itemRepository
     * @param FileHandler $fileHandler
     * @param array $config
     */
    public function __construct(
        ItemRepository $itemRepository,
        FileHandler $fileHandler,
        array $config
    ) {
        $this->itemRepository = $itemRepository;
        $this->fileHandler = $fileHandler;
        $this->config = $config[0];
    }

    /**
     * @param $id
     * @return Item|null
     */
    public function getItem($id)
    {
        return $this->itemRepository->find($id);
    }

    /**
     * @param array $queryParams
     * @return array
     */
    public function getAllItems($queryParams)
    {
        $queryBuilder = $this->itemRepository->createQueryBuilder('search');
        $queryBuilder->orderBy(
            isset($queryParams['sort']) ? $queryParams['sort'] : self::DEFAULT_SORT,
            isset($queryParams['direction']) ? $queryParams['direction'] : self::DEFAULT_SORT_DIRECTION
        );
        if (isset($queryParams['category_selector']) && isset($queryParams['category_selector']['category'])) {
            $queryBuilder->where('search.category = :category')
                ->setParameter('category', $queryParams['category_selector']['category']);
        }

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param Item $item
     */
    public function saveItem(Item $item)
    {
        $path = $this->config['image_upload_dir'];
        $images = $this->fileHandler->uploadFiles($path, $item->getImageFiles());
        $item->setImages($images);
        $this->itemRepository->save($item);
    }

    /**
     * @param User $user
     * @return array
     */
    public function getUserItems($user)
    {
        return $this->itemRepository->findBy(['createdBy' => $user]);
    }

    /**
     * @param $ids
     * @return \ItemBundle\Entity\Item[]
     */
    public function getItemsByIds($ids)
    {
        return $this->itemRepository->findByIds($ids);
    }
}
