<?php

namespace NotificationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use ItemBundle\Entity\Item;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use UserBundle\Entity\User;

/**
 * @ORM\Entity(repositoryClass="NotificationBundle\Entity\NotificationRepository")
 * @ORM\Table(name="notifications")
 * @ORM\HasLifecycleCallbacks
 */
class Notification
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Item
     * @ORM\ManyToOne(targetEntity="ItemBundle\Entity\Item")
     * @ORM\JoinColumn(name="item", referencedColumnName="id", onDelete="CASCADE")
     */
    private $item;

    /**
     * @var User
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="cart")
     * @ORM\JoinColumn(name="notification_by", referencedColumnName="id", onDelete="CASCADE")
     */
    private $notificationBy;

    /**
     * @var User
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", inversedBy="cart")
     * @ORM\JoinColumn(name="notification_for", referencedColumnName="id", onDelete="CASCADE")
     */
    private $notificationFor;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\PrePersist
     */
    public function setTimeStamps()
    {
        $this->createdAt = new \DateTime();
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param Item $item
     */
    public function setItem($item)
    {
        $this->item = $item;
    }

    /**
     * @return User
     */
    public function getNotificationBy()
    {
        return $this->notificationBy;
    }

    /**
     * @param User $notificationBy
     */
    public function setNotificationBy($notificationBy)
    {
        $this->notificationBy = $notificationBy;
    }

    /**
     * @return User
     */
    public function getNotificationFor()
    {
        return $this->notificationFor;
    }

    /**
     * @param User $notificationFor
     */
    public function setNotificationFor($notificationFor)
    {
        $this->notificationFor = $notificationFor;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

}
