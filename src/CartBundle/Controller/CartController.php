<?php

namespace CartBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CartController
 * @package CartBundle\Controller
 */
class CartController extends Controller
{
    /**
     * @Route("/add", name="cart.add_to_cart")
     * @Method("PUT")
     * @param Request $request
     * @return JsonResponse
     */
    public function addToCartAction(Request $request)
    {
        $id = $request->request->get('id');
        $item = $this->getItemService()->getItem($id);
        $cookie = $request->cookies->get('cart');
        $cart = $this->getCartService()->addItemToCart($item, $cookie);

        return  $response = new JsonResponse(
            [
                'status' => true,
                'cart' => [
                    'value' => $cart->getValue(),
                    'count' => $cart->getItemCount(),
                ]
            ]
        );;
    }


    /**
     * @Route("/my-cart", name="cart.my_cart")
     * @Method("GET")
     * @Template("CartBundle:Cart:my_cart.html.twig")
     */
    public function myCartAction(Request $request)
    {
        $user = $this->getUser();
        if ($user) {
            $entities = $this->getUser()->getCart()->getItems();
        } else {
            $cookie = $request->cookies->get('cart');
            $entities = $this->getCartService()->getCartFromSession($cookie)->getItems();
        }
        $config = $this->getParameter('item')[0];

        return [
            'items' => $entities,
            'config' => $config,
        ];
    }

    /**
     * @Route("/buy/{id}", name="cart.buy_item")
     * @Method("GET")
     */
    public function buyItemAction($id)
    {
        $this->getBuyItemService()->buyItem($id);

        return $this->redirectToRoute('cart.my_cart');
    }

    /**
     * @return \ItemBundle\Service\ItemService
     */
    private function getItemService()
    {
        return $this->get('item.service.item');
    }

    /**
     * @return \CartBundle\Service\CartService
     */
    private function getCartService()
    {
        return $this->get('cart.service.cart');
    }

    /**
     * @return \CartBundle\Service\BuyItemService
     */
    private function getBuyItemService()
    {
        return $this->get('cart.service.buy_item');
    }
}
