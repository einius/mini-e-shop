<?php

namespace CoreBundle\Service;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class FileHandler
 * @package CoreBundle\Service
 */
class FileHandler
{
    /**
     * @var $string
     */
    private $webDir;

    /**
     * @var UploadedFile|UploadedFile[]
     */
    private $files;

    /**
     * @var string
     */
    private $path;

    /**
     * FileHandler constructor.
     * @param $webDir
     */
    public function __construct($webDir)
    {
        $this->webDir = $webDir;
    }

    /**
     * @param string $path
     * @param UploadedFile|UploadedFile[] $files
     * @return array
     */
    public function uploadFiles($path, $files)
    {
        $this->setFiles($files);
        $this->setPath($path);

        return $this->uploadMultipleFiles($files);
    }


    /**
     * @param $files
     */
    private function setFiles($files)
    {
        $this->files = $files;
    }

    /**
     * @param $path
     */
    private function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @param UploadedFile[] $files
     * @return array
     */
    private function uploadMultipleFiles($files)
    {
        $fileNames = [];
        foreach ($files as $file) {
            if ($file) {
                $fileNames[] = $this->uploadSingleFile($file);
            }
        }

        return $fileNames;
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    private function uploadSingleFile(UploadedFile $file)
    {
        $fileName = $this->generateFileName($file);
        $file->move($this->webDir . $this->path, $fileName);

        return $fileName;
    }

    /**
     * @param UploadedFile $file
     * @return string
     */
    private function generateFileName(UploadedFile $file)
    {
        return sha1(uniqid(mt_rand(), true)) . '.' . $file->guessExtension();
    }
}
