$(document).ready(function(){
    var AddToCart = {
        elementIdentifier: '.add-to-cart',
        $cartValue: $('#cart-value'),
        $cartItemCount: $('#cart-item-count'),
        $buttons: null,
        $item: null,
        init: function() {
            this.cacheDom();
            this.bindEvents();
        },
        bindEvents: function() {
            var obj = this;
            obj.$buttons.click(function(){
                obj.$item = $(this);
                obj.addToCart();
            });
        },
        cacheDom: function(){
            this.$buttons = $(this.elementIdentifier).not('.disabled');
        },
        addToCart: function() {
            var obj = this;
            $.ajax({
                url: obj.$item.data('href'),
                type: 'PUT',
                data: {
                    id: obj.$item.data('id')
                },
                dataType: 'JSON'
            }).done(function(response){
                obj.handleResponse(response);
            }).fail(function(jqXHR, textStatus) {
                console.error("Request failed: " + textStatus);
            });
        },
        handleResponse: function(response) {
            if (!response.status) {
                return;
            }
            var obj = this;
            obj.$item.addClass('disabled');
            obj.$cartItemCount.text(response.cart.count);
            obj.$cartValue.text(response.cart.value);
        }
    };

    AddToCart.init();
});
