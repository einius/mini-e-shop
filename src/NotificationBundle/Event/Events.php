<?php

namespace NotificationBundle\Event;

/**
 * Class Events
 * @package NotificationBundle\Evebt
 */
class Events
{
    const ITEM_REQUESTED_TO_BUY = 'cart.event.item_added';
}
