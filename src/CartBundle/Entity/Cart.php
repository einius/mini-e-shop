<?php

namespace CartBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use ItemBundle\Entity\Item;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use UserBundle\Entity\User;

/**
 * @ORM\Entity(repositoryClass="CartBundle\Entity\CartRepository")
 * @ORM\Table(name="cart")
 * @UniqueEntity(fields={"user"})
 * @ORM\HasLifecycleCallbacks
 */
class Cart
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="ItemBundle\Entity\Item")
     *
     **/
    private $items;

    /**
     * @var float
     * @ORM\Column(name="price", type="decimal", scale=2, precision=10)
     */
    private $value = 0;

    /**
     * @var integer
     * @ORM\Column(type="integer", length=4)
     * @Assert\NotBlank()
     */
    private $itemCount = 0;


    /**
     * @var User
     * @Gedmo\Blameable(on="create")
     * @ORM\OneToOne(targetEntity="UserBundle\Entity\User", inversedBy="cart")
     * @ORM\JoinColumn(name="user", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     * Category constructor.
     */
    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return ArrayCollection|Item[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param Item $item
     * @return $this
     */
    public function addItem(Item $item) {
        $this->items->add($item);
        $this->value += $item->getPrice();
        $this->itemCount++;
        
        return $this;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return $this
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param float $value
     * @return Cart
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getItemCount()
    {
        return $this->itemCount;
    }

    /**
     * @param int $itemCount
     * @return Cart
     */
    public function setItemCount($itemCount)
    {
        $this->itemCount = $itemCount;
        return $this;
    }


}
