**How to run project**

1. Project runs on build in php server. you can run demo version using Symfony command `php bin/console server:run`
2. Run `composer install` command.
3. Update database connection settings in `app/config/parameters.yml`
4. Create database `php bin/console doctrine:database:create`
5. Update database schema `php bin/console doctrine:schema:update --force`
6. Load data fixtures `php bin/console doctrine:fixtures:load`


**How to run tests**
1. From root project dir execute `phpunit` command
