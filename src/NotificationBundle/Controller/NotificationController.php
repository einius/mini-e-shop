<?php

namespace NotificationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\User;

/**
 * Class CartController
 * @package CartBundle\Controller
 */
class NotificationController extends Controller
{
    /**
     * @Route("/", name="notification.list")
     * @Method("GET")
     * @Template("NotificationBundle:Notification:index.html.twig")
     */
    public function indexAction()
    {
        /**
         * @var User $user
         */
        $user = $this->getUser();

        return [
            'notifications' => $user->getNotifications(),
        ];
    }
}
