<?php

namespace ItemBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\All;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class ItemType
 * @package ItemBundle\Form
 */
class ItemType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('category', EntityType::class, [
                'label' => 'item.labels.category',
                'required' => true,
                'class' => 'ItemBundle\Entity\Category',
                'choice_label' => 'name',
                'placeholder' => '',
            ])
            ->add('name', TextType::class, [
                'label' => 'item.labels.name',
                'required' => true,
            ])
            ->add('price', TextType::class, [
                'label' => 'item.labels.price',
                'required' => true,
            ])
            ->add('description', TextareaType::class, [
                'label' => 'item.labels.description',
                'required' => true,
            ])
            ->add('imageFiles', FileType::class, [
                'label' => 'item.labels.images',
                'required' => false,
                'multiple' => true,
                'constraints' => [
                    new All([
                        'constraints' => [
                            new Image([
                                'maxSize' => 3145728,
                            ]),
                        ],
                    ]),
                ],
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'item.labels.submit'
            ])
        ;

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => 'ItemBundle\\Entity\\Item',
            ]);
    }
}
