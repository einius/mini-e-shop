<?php

namespace CartBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class CartControllerTest
 * @package CartBundle\Tests\Controller
 */
class CartControllerTest extends WebTestCase
{

    public function testLoginRequiredToBuyItemAction()
    {
        $client = static::createClient();
        $client->request('GET', '/cart/buy/1');
        $this->assertTrue($client->getResponse() instanceof RedirectResponse);
        $client->followRedirect();
        $this->assertContains('Username', $client->getResponse()->getContent());
        $this->assertContains('Password', $client->getResponse()->getContent());
        $this->assertRegExp('/\/login$/', $client->getRequest()->getUri());
    }
}
