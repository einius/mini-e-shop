<?php
namespace CartBundle\EventListener;

use CartBundle\Entity\Cart;
use CartBundle\Entity\RequestedToBuy;
use CartBundle\Service\CartService;
use FOS\UserBundle\Event\UserEvent;
use FOS\UserBundle\FOSUserEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use UserBundle\Entity\User;

/**
 * Class UserRegistrationListener
 * @package CartBundle\EventListener
 */
class UserSecurityListener implements EventSubscriberInterface
{
    /**
     * @var CartService
     */
    private $cartService;

    /**
     * UserSecurityListener constructor.
     * @param CartService $cartService
     */
    public function __construct(CartService $cartService)
    {
        $this->cartService = $cartService;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [
            FOSUserEvents::REGISTRATION_INITIALIZE => 'onRegistrationInitialise',
            'security.interactive_login' => 'mergeCarts',
        ];
    }

    /**
     * @param UserEvent $event
     */
    public function onRegistrationInitialise( UserEvent $event )
    {
        /**
         * @var User $user
         */
        $user = $event->getUser();
        $this->createCart($user);
        $this->createRequestedToBuy($user);
    }

    public function mergeCarts(InteractiveLoginEvent $event)
    {
        $cookie = $event->getRequest()->cookies->get('cart');
        $this->cartService->mergeCarts($cookie);
        $response = new Response();
        $response->headers->clearCookie('cart');
        $response->sendHeaders();
    }

    /**
     * @param User $user
     */
    private function createCart(User $user)
    {
        $cart = new Cart();
        $cart->setUser($user);
        $user->setCart($cart);
    }

    /**
     * @param User $user
     */
    private function createRequestedToBuy(User $user)
    {
        $requestedToBuy = new RequestedToBuy();
        $requestedToBuy->setUser($user);
        $user->setRequestedToBuy($requestedToBuy);
    }
}
