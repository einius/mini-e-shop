<?php

namespace ItemBundle\Controller;

use ItemBundle\Entity\Item;
use ItemBundle\Form\CategorySelectorType;
use ItemBundle\Form\ItemType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ItemController
 * @package ItemBundle\Controller
 */
class ItemController extends Controller
{

    /**
     * @Route("/", name="item.list")
     * @Method("GET")
     * @Template()
     */
    public function indexAction(Request $request)
    {
        $entities = $this->getItemService()->getAllItems($request->query->all());
        $config = $this->getParameter('item')[0];
        $form = $this->createForm(CategorySelectorType::class, null, ['method' => 'GET']);
        return [
            'items' => $entities,
            'config' => $config,
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/my-items", name="item.user_items")
     * @Method("GET")
     * @Template("ItemBundle:Item:my_items.html.twig")
     */
    public function userItemsAction()
    {
        $entities = $this->getItemService()->getUserItems($this->getUser());
        $config = $this->getParameter('item')[0];

        return [
            'items' => $entities,
            'config' => $config,
        ];
    }

    /**
     * @Route("/show/{id}", name="item.show")
     * @Method("GET")
     * @Template()
     * @param int $id
     * @return array
     */
    public function showAction($id)
    {
        $entity = $this->getItemService()->getItem($id);
        $config = $this->getParameter('item')[0];

        return [
            'item' => $entity,
            'config' => $config,
        ];
    }

    /**
     * @Route("/create", name="item.create")
     * @Method("GET")
     * @Template()
     */
    public function createAction()
    {
        $form = $this->createForm(ItemType::class, new Item());

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @Route("/create")
     * @Method("POST")
     * @Template("ItemBundle:Item:create.html.twig")
     */
    public function saveAction(Request $request)
    {
        $form = $this->createForm(ItemType::class, new Item());
        $form->handleRequest($request);
        if ($form->isValid()) {
            $entity = $form->getData();
            $this->getItemService()->saveItem($entity);

            return $this->redirectToIndex();
        }

        return [
            'form' => $form->createView(),
        ];
    }

    /**
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    private function redirectToIndex()
    {
        return $this->redirectToRoute('item.list');
    }

    /**
     * @return \ItemBundle\Service\ItemService
     */
    private function getItemService()
    {
        return $this->get('item.service.item');
    }
}
