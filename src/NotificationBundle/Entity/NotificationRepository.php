<?php

namespace NotificationBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * Class NotificationRepository
 * @package NotificationBundle\Entity
 */
class NotificationRepository extends EntityRepository
{
    /**
     * @param Notification $entity
     */
    public function save(Notification $entity)
    {
        $this->_em->persist($entity);
        $this->_em->flush($entity);
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        return parent::getEntityManager();
    }
}
