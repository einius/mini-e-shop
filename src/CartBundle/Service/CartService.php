<?php
namespace CartBundle\Service;
use CartBundle\Entity\Cart;
use CartBundle\Entity\CartRepository;
use CartBundle\Model\SessionCart;
use ItemBundle\Entity\Item;
use ItemBundle\Service\ItemService;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use UserBundle\Entity\User;

/**
 * Class CartService
 * @package CartBundle\Service
 */
class CartService
{
    /**
     * @var CartRepository
     */
    private $cartRepository;

    /**
     * @var ItemService
     */
    private $itemService;

    /**
     * @var TokenStorage
     */
    private $tokenStorage;


    /**
     * CartService constructor.
     * @param CartRepository $cartRepository
     * @param ItemService $itemService
     * @param TokenStorage $tokenStorage
     */
    public function __construct(
        CartRepository $cartRepository,
        ItemService $itemService,
        TokenStorage $tokenStorage
    ) {
        $this->cartRepository = $cartRepository;
        $this->itemService = $itemService;
        $this->tokenStorage = $tokenStorage;
    }


    /**
     * @param Item $item
     * @param $cartCookie
     * @return Cart
     */
    public function addItemToCart(Item $item, $cartCookie)
    {
        $user = $this->getUser();
        if ($user instanceof User) {
            $cart = $this->saveCartToDatabase($item, $user);
        } else {
            $cart = $this->createSessionCart($item, $cartCookie);
            $this->createCartCookie($cart);
        }

        return $cart;
    }

    /**
     * @return User
     */
    private function getUser()
    {
        return $this->tokenStorage->getToken()->getUser();
    }

    /**
     * @return Cart
     */
    private function createCart(User $user)
    {
        $cart = new Cart();
        $cart->setUser($user);
        $this->cartRepository->save($cart);

        return $cart;
    }

    /**
     * @param $item
     * @param $cookie
     * @return SessionCart
     */
    private function createSessionCart($item, $cookie)
    {
        $cart = new SessionCart($cookie);
        $cart->addItem($item);

        return $cart;
    }

    /**
     * @param Item $item
     * @param User $user
     * @return Cart
     */
    private function saveCartToDatabase($item, $user)
    {
        $cart = $user->getCart();
        if (!$cart) {
            $cart = $this->createCart($user);
        }

        $cart->addItem($item);
        $this->cartRepository->save($cart);

        return $cart;
    }

    public function getCartFromSession($cookie = null)
    {
        $cart = new SessionCart($cookie);
        if (is_array($cart->getItems())) {
            $items = $this->itemService->getItemsByIds($cart->getItems());
            $cart->setItems($items);
        }

        return $cart;
    }


    /**
     * @param Cookie $cookie
     */
    public function mergeCarts($cookie)
    {
        $user = $this->getUser();
        $cart = $user->getCart();
        $sessionCart = $this->getCartFromSession($cookie);

        foreach ($sessionCart->getItems() as $item) {
            if (!$cart->getItems()->contains($item)) {
                $cart->addItem($item);
            }
        }

        $this->cartRepository->save($cart);
    }

    /**
     * @param $cart
     */
    private function createCartCookie($cart)
    {
        $cookie = new Cookie('cart', $cart->getJson(), time() + 3600 * 240);
        $response = new Response();
        $response->headers->setCookie($cookie);
        $response->sendHeaders();
    }
}
