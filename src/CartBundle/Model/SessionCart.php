<?php
namespace CartBundle\Model;
use ItemBundle\Entity\Item;

/**
 * Class SessionCart
 * @package CartBundle\Model
 */
class SessionCart
{
    /**
     * @var array
     */
    public $items = [];

    /**
     * @var int
     */
    public $value = 0;

    /**
     * @var int
     */
    public $itemCount = 0;

    /**
     * @param string $data
     */
    public function __construct($data)
    {
        $data = json_decode($data, true);
        if ($data) {
            $this->items = $data['items'];
            $this->value = $data['value'];
            $this->itemCount = $data['itemCount'];
        }
    }

    /**
     * @return string
     */
    public function getJson()
    {
        $data = [
            'items' => $this->items,
            'value' => $this->value,
            'itemCount' => $this->itemCount,
        ];

        return json_encode($data);
    }

    /**
     * @return array
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param array $items
     */
    public function setItems($items)
    {
        $this->items = $items;
    }

    /**
     * @param Item $item
     */
    public function addItem(Item $item)
    {
        $this->items[$item->getId()] = $item->getId();
        $this->value += $item->getPrice();
        $this->itemCount++;
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param int $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return int
     */
    public function getItemCount()
    {
        return $this->itemCount;
    }

    /**
     * @param int $itemCount
     */
    public function setItemCount($itemCount)
    {
        $this->itemCount = $itemCount;
    }
}
