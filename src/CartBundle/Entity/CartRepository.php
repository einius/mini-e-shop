<?php

namespace CartBundle\Entity;

use CartBundle\Entity\Cart;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * Class CartRepository
 * @package CartBundle\Entity
 */
class CartRepository extends EntityRepository
{
    /**
     * @param Cart $entity
     */
    public function save(Cart $entity)
    {
        $this->_em->persist($entity);
        $this->_em->flush($entity);
    }
}
