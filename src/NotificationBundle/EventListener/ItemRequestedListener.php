<?php
namespace NotificationBundle\EventListener;

use ItemBundle\Entity\Item;
use NotificationBundle\Entity\Notification;
use NotificationBundle\Event\Events;
use NotificationBundle\Service\NotificationService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

/**
 * Class UserRegistrationListener
 * @package CartBundle\EventListener
 */
class ItemRequestedListener implements EventSubscriberInterface
{
    /**
     * @var NotificationService
     */
    private $notificationService;

    /**
     * ItemRequestedListener constructor.
     * @param NotificationService $notificationService
     */
    public function __construct(NotificationService $notificationService)
    {
        $this->notificationService = $notificationService;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return [Events::ITEM_REQUESTED_TO_BUY => 'sendItemRequestedNotification'];
    }

    /**
     * @param GenericEvent $event
     */
    public function sendItemRequestedNotification(GenericEvent $event)
    {
        /**
         * @var Item $item
         */
        $item = $event->getSubject();
        $this->notificationService->createNotification($item);
    }


}
