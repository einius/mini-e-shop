<?php

namespace CartBundle\Entity;

use CartBundle\Entity\Cart;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * Class RequestedToBuyRepository
 * @package CartBundle\Entity
 */
class RequestedToBuyRepository extends EntityRepository
{
    /**
     * @param RequestedToBuy $entity
     */
    public function save(RequestedToBuy $entity)
    {
        $this->_em->persist($entity);
        $this->_em->flush($entity);
    }
}
