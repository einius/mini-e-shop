<?php

namespace ItemBundle\Test\Service;
use CoreBundle\Service\FileHandler;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\QueryBuilder;
use ItemBundle\Entity\Item;
use ItemBundle\Entity\ItemRepository;
use ItemBundle\Service\ItemService;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;


/**
 * Class ItemServiceTest
 * @package ItemBundle\Test\Service
 */
class ItemServiceTest extends TestCase
{
    /**
     * @var PHPUnit_Framework_MockObject_MockObject|ItemRepository
     */
    private $itemRepositoryMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|FileHandler
     */
    private $fileHandlerMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|QueryBuilder
     */
    private $queryBuilderMock;

    /**
     * @var PHPUnit_Framework_MockObject_MockObject|AbstractQuery
     */
    private $abstractQueryMock;

    /**
     * @var array
     */
    private $config = [];

    public function setUp()
    {
        $this->itemRepositoryMock = $this->getMockBuilder(ItemRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->fileHandlerMock = $this->getMockBuilder(FileHandler::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->queryBuilderMock = $this->getMockBuilder(QueryBuilder::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->abstractQueryMock = $this->getMockBuilder(AbstractQuery::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->config[0] = [
            'image_upload_dir' => '/uploads/images/',
        ];
    }

    public function tearDown()
    {

    }

    public function testInstance()
    {
        $itemService = $this->getInstance();
        $this->assertInstanceOf(ItemService::class, $itemService);
    }

    public function testItemSave()
    {
        $item = new Item();
        $this->itemRepositoryMock
            ->expects($this->once())
            ->method('save')
            ->with($item);
        $itemService = $this->getInstance();
        $itemService->saveItem($item);
    }

    public function testGetItem()
    {
        $id = 1;
        $this->itemRepositoryMock
            ->expects($this->once())
            ->method('find')
            ->with($id);
        $itemService = $this->getInstance();
        $itemService->getItem($id);
    }

    public function testGetAllItems()
    {
        $items = [];
        $items[] = new Item();
        $items[] = new Item();

        $queryParams = [];
        $this->itemRepositoryMock
            ->expects($this->once())
            ->method('createQueryBuilder')
            ->willReturn($this->queryBuilderMock)
            ->with('search', null);
        $this->queryBuilderMock
            ->expects($this->once())
            ->method('getQuery')
            ->willReturn($this->abstractQueryMock);
        $this->abstractQueryMock
            ->expects($this->once())
            ->method('getResult')
            ->willReturn($items);
        $itemService = $this->getInstance();
        $result = $itemService->getAllItems($queryParams);
        $this->assertInternalType('array',$result);
        $this->assertContainsOnlyInstancesOf(Item::class, $items);
    }

    /**
     * @return ItemService
     */
    private function getInstance()
    {
        return new ItemService(
            $this->itemRepositoryMock,
            $this->fileHandlerMock,
            $this->config
        );
    }

}
