<?php

namespace CoreBundle\Twig;
use Doctrine\ORM\PersistentCollection;

/**
 * Class GridExtension
 * @package CoreBundle\Twig
 */
class ArrayExtension extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('inArray', [$this, 'inArray']),
        ];
    }

    /**
     * @param mixed $needle
     * @param array|PersistentCollection $haystack
     * @return bool
     */
    public function inArray($needle, $haystack)
    {
        if ($haystack instanceof PersistentCollection) {
            $haystack = $haystack->toArray();
        } elseif (!is_array($haystack)) {
            return false;
        }

        return in_array($needle, $haystack);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'array_extension';
    }
}
