<?php

namespace CartBundle\Twig;
use CartBundle\Service\CartService;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\User;

/**
 * Class GridExtension
 * @package CoreBundle\Twig
 */
class CartExtension extends \Twig_Extension
{
    /**
     * @var CartService
     */
    private $cartService;

    /**
     * CartExtension constructor.
     * @param CartService $cartService
     */
    public function __construct(CartService $cartService)
    {
        $this->cartService = $cartService;
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('getCartItems', [$this, 'getCartItems']),
            new \Twig_SimpleFunction('getCart', [$this, 'getCart']),
        ];
    }

    /**
     * @param null|User $user
     * @param Request $request
     * @return array
     */
    public function getCartItems($request, $user = null)
    {
        if (!$user) {
            $cookie = $request->cookies->get('cart');

            return (array)$this->cartService->getCartFromSession($cookie)->getItems();
        }

        return $user->getCart()->getItems();
    }

    /**
     * @param null|User $user
     * @param Request $request
     * @return array
     */
    public function getCart($request, $user = null)
    {
        if (!$user) {
            $cookie = $request->cookies->get('cart');

            return $this->cartService->getCartFromSession($cookie);
        }

        return $user->getCart();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cart_extension';
    }
}
