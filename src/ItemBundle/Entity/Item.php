<?php

namespace ItemBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use UserBundle\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="ItemBundle\Entity\ItemRepository")
 * @ORM\Table(name="items")
 * @ORM\HasLifecycleCallbacks
 */
class Item
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;
    /**
     * @var float
     * @ORM\Column(name="price", type="decimal", scale=2, precision=10)
     * @Assert\NotBlank()
     * @Assert\Regex(
     *     pattern="/^\d+(,\d{1,2})?$/",
     *     match=false,
     *     message="Invalid price"
     * )
     */
    private $price;

    /**
     * @var string
     * @ORM\Column(type="text")
     * @Assert\NotBlank()
     */
    private $description;

    /**
     * @var User
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(name="created_by", referencedColumnName="id", onDelete="CASCADE")
     */
    private $createdBy;

    /**
     * @var Category
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="ItemBundle\Entity\Category", inversedBy="items", cascade={"persist"})
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     **/
    private $category;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $images;

    /**
     * @var UploadedFile[]
     */
    private $imageFiles;



    /**
     * @ORM\PreUpdate
     * @ORM\PrePersist
     */
    public function setTimeStamps()
    {
        if ($this->getId() == null) {
            $this->createdAt = new \DateTime();
        } else {
            $this->updatedAt = new \DateTime();
        }
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Item
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Item
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     * @return Item
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     * @return Item
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return Item
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Item
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return User
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param User $createdBy
     * @return Item
     */
    public function setCreatedBy(User $createdBy)
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param Category $category
     * @return Item
     */
    public function setCategory(Category $category)
    {
        $this->category = $category;
        return $this;
    }

    /**
     * @return UploadedFile[]
     */
    public function getImageFiles()
    {
        return $this->imageFiles;
    }

    /**
     * @param UploadedFile[] $imageFiles
     * @return $this
     */
    public function setImageFiles($imageFiles)
    {
        $this->imageFiles = $imageFiles;
        return $this;
    }

    /**
     * @return array
     */
    public function getImages()
    {
        if (!is_array($this->images)) {
            return (array)json_decode($this->images, true);
        }

        return $this->images;
    }

    /**
     * @param string|array $images
     * @return $this
     */
    public function setImages($images)
    {
        if (is_array($images)) {
            $images = json_encode($images);
        }
        $this->images = $images;

        return $this;
    }
}
