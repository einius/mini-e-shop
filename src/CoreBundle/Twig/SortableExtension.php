<?php

namespace CoreBundle\Twig;
use Doctrine\ORM\PersistentCollection;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class SortableExtension
 * @package CoreBundle\Twig
 */
class SortableExtension extends \Twig_Extension
{
    /**
     * @var Router
     */
    private $router;

    /**
     * SortableExtension constructor.
     * @param Router $router
     */
    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('sortable', [$this, 'sortable'], ['needs_environment' => true]),
        ];
    }

    /**
     * @param mixed $needle
     * @param array|PersistentCollection $haystack
     * @return bool
     */
    public function sortable(\Twig_Environment $env, $sortBy, $translationId)
    {
        /**
         * @var Request $request
         */
        $request = $env->getGlobals()['app']->getRequest();
        $routeName = $request->attributes->get('_route');
        $queryParams = $request->query->all();
        $queryParams['direction'] = $this->toggleDirection(isset($queryParams['direction']) ? $queryParams['direction'] : null);
        $queryParams['sort'] = $sortBy;
        $url = $this->router->generate($routeName, $queryParams);

        return $url;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sortable_extension';
    }

    /**
     * @param string $direction
     * @return string
     */
    private function toggleDirection($direction)
    {
        if ($direction == 'ASC') {
            return 'DESC';
        }

        return 'ASC';
    }
}
