<?php

namespace NotificationBundle\Service;

use ItemBundle\Entity\Item;
use ItemBundle\Entity\ItemRepository;
use NotificationBundle\Entity\Notification;
use NotificationBundle\Entity\NotificationRepository;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use UserBundle\Entity\User;

/**
 * Class NotificationService
 * @package NotificationBundle\Service
 */
class NotificationService
{
    /**
     * @var NotificationRepository
     */
    private $notificationRepo;

    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * @param NotificationRepository $notificationRepo
     * @param TokenStorage $tokenStorage
     */
    public function __construct(
        NotificationRepository $notificationRepo,
        TokenStorage $tokenStorage
    ) {
        $this->notificationRepo = $notificationRepo;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param Item $item
     */
    public function createNotification(Item $item)
    {
        $notification = new Notification();
        $notification->setItem($item);
        $user = $this->getUser();
        $notification->setNotificationBy($user);
        $notification->setNotificationFor($item->getCreatedBy());
        $this->notificationRepo->save($notification);
        $user->addNotification($notification);
        $this->notificationRepo->getEntityManager()->persist($user);
        $this->notificationRepo->getEntityManager()->flush($user);

    }

    /**
     * @return User
     */
    private function getUser()
    {
        return $this->tokenStorage->getToken()->getUser();
    }
}
