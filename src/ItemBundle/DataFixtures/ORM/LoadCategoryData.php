<?php
namespace ItemBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use ItemBundle\Entity\Category;

/**
 * Class LoadCategoryData
 * @package ItemBundle\DataFixtures\ORM
 */
class LoadCategoryData implements FixtureInterface
{

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $category1 = new Category();
        $category1->setName('Phones');
        $manager->persist($category1);

        $category2 = new Category();
        $category2->setName('Laptops');
        $manager->persist($category2);

        $category3 = new Category();
        $category3->setName('Desktop computers');
        $manager->persist($category3);

        $category4 = new Category();
        $category4->setName('Photo cameras');
        $manager->persist($category4);

        $category5 = new Category();
        $category5->setName('Video cameras');
        $manager->persist($category5);

        $manager->flush();
    }
}
