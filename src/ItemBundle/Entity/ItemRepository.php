<?php

namespace ItemBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use ItemBundle\Entity\Item;

/**
 * Class ItemRepository
 * @package ItemBundle\Entity
 */
class ItemRepository extends EntityRepository
{
    /**
     * @param Item $entity
     */
    public function save(Item $entity)
    {
        $this->_em->persist($entity);
        $this->_em->flush($entity);
    }

    /**
     * @param array $ids
     * @return Item[]
     */
    public function findByIds(array $ids) {
        $qb = $this->createQueryBuilder('search')
            ->select('search')
            ->where('search.id IN (:ids)')
            ->setParameter('ids', $ids);

        return $qb->getQuery()->getResult();
    }
}
