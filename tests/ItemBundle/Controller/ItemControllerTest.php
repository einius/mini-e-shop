<?php

namespace ItemBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class ItemControllerTest
 * @package ItemBundle\Tests\Controller
 */
class ItemControllerTest extends WebTestCase
{
    public function testLoginRequiredToCreateItemAction()
    {
        $client = static::createClient();
        $client->request('GET', '/item/create');
        $this->assertTrue($client->getResponse() instanceof RedirectResponse);
        $client->followRedirect();
        $this->assertContains('Username', $client->getResponse()->getContent());
        $this->assertContains('Password', $client->getResponse()->getContent());
        $this->assertRegExp('/\/login$/', $client->getRequest()->getUri());
    }
}
