<?php

namespace CoreBundle\Twig;

/**
 * Class GridExtension
 * @package CoreBundle\Twig
 */
class GridExtension extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('grid', [$this, 'grid']),
        ];
    }

    /**
     * @param array $items
     * @param int $columnCount
     * @return array
     */
    public function grid($items, $columnCount)
    {
        $array = [];
        $rowArray = [];
        $count = 0;
        $index = 0;
        foreach ($items as $item) {
            $index++;
            if ($count == $columnCount) {
                $array[] = $rowArray;
                $rowArray = [];
                $count = 0;
            }
            $rowArray[] = $item;
            $count++;
            $itemCount = count($items);
            if ($index == $itemCount) {
                $array[] = $rowArray;
            }
        }

        return $array;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'grid_extension';
    }
}
